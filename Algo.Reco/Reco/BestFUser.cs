﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo
{
    /// <summary>
    /// Foe or Friend.
    /// </summary>
    public class BestFUser
    {
        public readonly User FUser;
        public readonly Double Similarity;

        public BestFUser( User u, double s )
        {
            FUser = u;
            Similarity = s;
        }
    }



}
