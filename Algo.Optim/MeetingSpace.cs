﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo.Optim
{
    public class MeetingSpace : SolutionSpace
    {
        readonly FlightDatabase _db;

        public readonly Airport Location;
        public readonly DateTime BusMaxStartTime;
        public readonly DateTime BegMeeting;
        public readonly DateTime EndMeeting;
        public readonly Guest[] Guests;
            
        IReadOnlyList<SimpleFlight>[] _arrivals;
        IReadOnlyList<SimpleFlight>[] _departures;

        public MeetingSpace( int seed, string databasePath )
            : base( seed )
        {
            _db = new FlightDatabase( databasePath );

            Location = Airport.FindByCode( "LHR" );
            BegMeeting = new DateTime( 2010, 7, 27, 18, 0, 0 );
            BusMaxStartTime = BegMeeting.AddHours( -1 );
            EndMeeting = new DateTime( 2010, 8, 3, 14, 0, 0 );
            Guests = new Guest[]
            {
                new Guest(){ Name = "Ingrid", Location=Airport.FindByCode("BER") },
                new Guest(){ Name = "Joël", Location=Airport.FindByCode("CDG") },
                new Guest(){ Name = "Marius", Location=Airport.FindByCode("MRS") },
                new Guest(){ Name = "Vanessa", Location=Airport.FindByCode("LYS") },
                new Guest(){ Name = "Kate", Location=Airport.FindByCode("MAN") },
                new Guest(){ Name = "Maria", Location=Airport.FindByCode("BIO") },
                new Guest(){ Name = "Bob", Location=Airport.FindByCode("JFK") },
                new Guest(){ Name = "Ahmed", Location=Airport.FindByCode("TUN") },
                new Guest(){ Name = "Chiara", Location=Airport.FindByCode("MXP") }
            };
            _arrivals = new IReadOnlyList<SimpleFlight>[9];
            for( int i = 0; i < 9; ++i )
            {
                var source = Guests[i].Location;
                var target = Location;
                var candidates = _db.GetFlights( BegMeeting.Date, source, target )
                                     .Concat( _db.GetFlights( BegMeeting.Date.AddDays( -1 ), source, target ) );
                var interesting = candidates.Where( f => f.ArrivalTime < BusMaxStartTime )
                                            .Where( f => f.ArrivalTime >= BusMaxStartTime.AddHours( -6 ) )
                                            .OrderBy( f => f.ArrivalTime );
                _arrivals[i] = interesting.ToArray();
            }
            _departures = new IReadOnlyList<SimpleFlight>[9];
            for( int i = 0; i < 9; ++i )
            {
                var source = Location;
                var target = Guests[i].Location;
                var candidates = _db.GetFlights( EndMeeting.Date, source, target );
                var interesting = candidates.Where( f => f.DepartureTime > EndMeeting.AddHours( 1 ) )
                                            .Where( f => f.DepartureTime <= EndMeeting.AddHours( 7 ) )
                                            .OrderBy( f => f.DepartureTime );
                _departures[i] = interesting.ToArray();
            }
            int[] dimensions = new int[18];
            for( int i = 0; i < 9; ++i )
            {
                dimensions[i] = _arrivals[i].Count;
                dimensions[i + 9] = _departures[i].Count;
            }
            Initialize( dimensions );
        }

        public IReadOnlyList<SimpleFlight> ArrivalFlightsFor( int iGuest )
        {
            return _arrivals[iGuest];
        }

        public IReadOnlyList<SimpleFlight> DepartureFlightsFor( int iGuest )
        {
            return _departures[iGuest];
        }

        protected override SolutionInstance DoCreateInstance( int[] current )
        {
            return new MeetingInstance( this, current );
        }

    }
}
