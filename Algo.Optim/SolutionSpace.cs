﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo.Optim
{
    public abstract class SolutionSpace
    {
        int[] _dimensions;
        SolutionInstance _bestEverSeen;
        readonly Random _random;

        public SolutionSpace( int seed )
        {
            _random = new Random( seed );
        }

        public SolutionInstance BestEverSeen 
        {
            get { return _bestEverSeen; } 
        }

        protected void Initialize( int[] dimensions )
        {
            if( dimensions.Any( i => i == 0 ) )
            {
                throw new ArgumentException( "This is not a solvable problem." );
            }
            _dimensions = dimensions.ToArray();
        }

        public IReadOnlyList<int> Dimensions { get { return _dimensions; } }

        public double Cardinality
        {
            get
            {
                double c = 1;
                for( int i = 0; i < _dimensions.Length; ++i )
                {
                    c *= _dimensions[i];
                }
                return c;
            }
        }

        public void BruteForce( int limit = 0 )
        {
            int[] current = new int[_dimensions.Length];
            // 1 - Obtain a concrete SolutionInstance for this current vector.
            // 2 - If the cost of this SI is lower than BestEverSeen, set BestEverSeen to it.
            // 3 - Compute the next current vector.
            // 4 - Goto 1.
            do
            {
                SolutionInstance i = CreateInstance( current );
                current = Next( current );
                if( limit > 0 && --limit == 0 ) return;
            }
            while( current != null );
        }

        public void RandomWalk( int limit )
        {
            while( --limit > 0 )
            {
                CreateRandomInstance();
            }
        }

        int[] Next( IReadOnlyList<int> current )
        {
            var next = new int[current.Count];
            int i = 0; 
            while( i < current.Count )
            {
                next[i] = current[i] + 1;
                if( next[i] < _dimensions[i] ) break;
                next[i] = 0;
                ++i;
            }
            if( i == current.Count ) return null;
            while( i < current.Count )
            {
                next[i] = current[i];
                ++i;
            }
            return next;
        }

        int[] Random()
        {
            var alea = new int[_dimensions.Length];
            for(int i = 0; i < _dimensions.Length; ++i )
            {
                alea[i] = _random.Next( _dimensions[i] );
            }
            return alea;
        }

        SolutionInstance CreateRandomInstance()
        {
            return CreateInstance( Random() );
        }

        SolutionInstance CreateInstance( int[] current )
        {
            SolutionInstance i = DoCreateInstance( current );
            if( _bestEverSeen == null || _bestEverSeen.Cost > i.Cost ) _bestEverSeen = i;
            return i;
        }

        protected abstract SolutionInstance DoCreateInstance( int[] current );

    }
}
