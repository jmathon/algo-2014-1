﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Algo.Optim;
using System.Xml.Linq;
using System.IO;
using System.Diagnostics;

namespace Algo.Tests
{
    [TestFixture]
    public class Optim
    {
        [Test]
        public void CreateMeetingSpace()
        {
            string databasePath = Path.Combine( TestHelper.SolutionFolder, @"ThirdParty\FlightData\" );
            var space = new MeetingSpace( 0, databasePath );
            Console.WriteLine( "Cardinality = {0}", space.Cardinality );
        }

        [Test]
        public void BruteForceSpeed()
        {
            string databasePath = Path.Combine( TestHelper.SolutionFolder, @"ThirdParty\FlightData\" );
            var space = new MeetingSpace( 0, databasePath );
            Stopwatch w = new Stopwatch();
            w.Start();
            space.BruteForce( 100 * 1000 );
            w.Stop();
            Console.WriteLine( "Best Cost = {0} - Last {1} ms", space.BestEverSeen.Cost, w.ElapsedMilliseconds );
        }

        [Test]
        public void RandomWalk()
        {
            string databasePath = Path.Combine( TestHelper.SolutionFolder, @"ThirdParty\FlightData\" );
            var space = new MeetingSpace( 1, databasePath );
            space.RandomWalk( 100 * 1000 );
            Console.WriteLine( "Best Cost = {0}", space.BestEverSeen.Cost );
        }

        [Test]
        public void DifferenceBetweenBruteForceAndRandom()
        {
            string databasePath = Path.Combine( TestHelper.SolutionFolder, @"ThirdParty\FlightData\" );
            var space1 = new MeetingSpace( 0, databasePath );
            space1.BruteForce( 100 * 1000 );

            var space2 = new MeetingSpace( 0, databasePath );
            space2.RandomWalk( 100 * 1000 );

            Assert.That( space2.BestEverSeen.Cost, Is.LessThan( space1.BestEverSeen.Cost ) );
        }

        [Test]
        public void GetFlights()
        {
            FlightDatabase db = new FlightDatabase( Path.Combine( TestHelper.SolutionFolder, @"ThirdParty\FlightData\" ) );

            {
                var f0 = db.GetFlights( new DateTime( 2010, 7, 26 ), Airport.FindByCode( "BER" ), Airport.FindByCode( "LHR" ) );
                var f1 = db.GetFlights( new DateTime( 2010, 7, 26 ), Airport.FindByCode( "CDG" ), Airport.FindByCode( "LHR" ) );
                var f2 = db.GetFlights( new DateTime( 2010, 7, 26 ), Airport.FindByCode( "MRS" ), Airport.FindByCode( "LHR" ) );
                var f3 = db.GetFlights( new DateTime( 2010, 7, 26 ), Airport.FindByCode( "LYS" ), Airport.FindByCode( "LHR" ) );
                var f4 = db.GetFlights( new DateTime( 2010, 7, 26 ), Airport.FindByCode( "MAN" ), Airport.FindByCode( "LHR" ) );
                var f5 = db.GetFlights( new DateTime( 2010, 7, 26 ), Airport.FindByCode( "BIO" ), Airport.FindByCode( "LHR" ) );
                var f6 = db.GetFlights( new DateTime( 2010, 7, 26 ), Airport.FindByCode( "JFK" ), Airport.FindByCode( "LHR" ) );
                var f7 = db.GetFlights( new DateTime( 2010, 7, 26 ), Airport.FindByCode( "TUN" ), Airport.FindByCode( "LHR" ) );
                var f8 = db.GetFlights( new DateTime( 2010, 7, 26 ), Airport.FindByCode( "MXP" ), Airport.FindByCode( "LHR" ) );
            }
            {
                var f0 = db.GetFlights( new DateTime( 2010, 7, 27 ), Airport.FindByCode( "BER" ), Airport.FindByCode( "LHR" ) );
                var f1 = db.GetFlights( new DateTime( 2010, 7, 27 ), Airport.FindByCode( "CDG" ), Airport.FindByCode( "LHR" ) );
                var f2 = db.GetFlights( new DateTime( 2010, 7, 27 ), Airport.FindByCode( "MRS" ), Airport.FindByCode( "LHR" ) );
                var f3 = db.GetFlights( new DateTime( 2010, 7, 27 ), Airport.FindByCode( "LYS" ), Airport.FindByCode( "LHR" ) );
                var f4 = db.GetFlights( new DateTime( 2010, 7, 27 ), Airport.FindByCode( "MAN" ), Airport.FindByCode( "LHR" ) );
                var f5 = db.GetFlights( new DateTime( 2010, 7, 27 ), Airport.FindByCode( "BIO" ), Airport.FindByCode( "LHR" ) );
                var f6 = db.GetFlights( new DateTime( 2010, 7, 27 ), Airport.FindByCode( "JFK" ), Airport.FindByCode( "LHR" ) );
                var f7 = db.GetFlights( new DateTime( 2010, 7, 27 ), Airport.FindByCode( "TUN" ), Airport.FindByCode( "LHR" ) );
                var f8 = db.GetFlights( new DateTime( 2010, 7, 27 ), Airport.FindByCode( "MXP" ), Airport.FindByCode( "LHR" ) );
            }

            {
                var f0 = db.GetFlights( new DateTime( 2010, 8, 3 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "BER" ) );
                var f1 = db.GetFlights( new DateTime( 2010, 8, 3 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "CDG" ) );
                var f2 = db.GetFlights( new DateTime( 2010, 8, 3 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "MRS" ) );
                var f3 = db.GetFlights( new DateTime( 2010, 8, 3 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "LYS" ) );
                var f4 = db.GetFlights( new DateTime( 2010, 8, 3 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "MAN" ) );
                var f5 = db.GetFlights( new DateTime( 2010, 8, 3 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "BIO" ) );
                var f6 = db.GetFlights( new DateTime( 2010, 8, 3 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "JFK" ) );
                var f7 = db.GetFlights( new DateTime( 2010, 8, 3 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "TUN" ) );
                var f8 = db.GetFlights( new DateTime( 2010, 8, 3 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "MXP" ) );
            }
            {
                var f0 = db.GetFlights( new DateTime( 2010, 8, 4 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "BER" ) );
                var f1 = db.GetFlights( new DateTime( 2010, 8, 4 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "CDG" ) );
                var f2 = db.GetFlights( new DateTime( 2010, 8, 4 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "MRS" ) );
                var f3 = db.GetFlights( new DateTime( 2010, 8, 4 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "LYS" ) );
                var f4 = db.GetFlights( new DateTime( 2010, 8, 4 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "MAN" ) );
                var f5 = db.GetFlights( new DateTime( 2010, 8, 4 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "BIO" ) );
                var f6 = db.GetFlights( new DateTime( 2010, 8, 4 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "JFK" ) );
                var f7 = db.GetFlights( new DateTime( 2010, 8, 4 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "TUN" ) );
                var f8 = db.GetFlights( new DateTime( 2010, 8, 4 ), Airport.FindByCode( "LHR" ), Airport.FindByCode( "MXP" ) );
            }


        }

    }
}
