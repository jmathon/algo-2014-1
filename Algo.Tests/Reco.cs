﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;

namespace Algo.Tests
{
    [TestFixture]
    public class Reco
    {
        static string _badDataPath = Path.Combine( TestHelper.SolutionFolder, @"ThirdParty\MovieData\MovieLens\" );
        static string _goodDataPath = Path.Combine( TestHelper.SolutionFolder, @"ThirdParty\MovieData\" );
        static string _subsetDataPath = Path.Combine( TestHelper.SolutionFolder, @"ThirdParty\MovieData\MovieSubset" );

        [Test]
        public void CorrectData()
        {
            Dictionary<int, Movie> firstMovies;
            Dictionary<int, List<Movie>> duplicateMovies;
            Movie.ReadMovies( Path.Combine( _badDataPath, "movies.dat" ), out firstMovies, out duplicateMovies );
            int idMovieMin = firstMovies.Keys.Min();
            int idMovieMax = firstMovies.Keys.Max();
            Console.WriteLine( "{3} Movies from {0} to {1}, {2} duplicates.", idMovieMin, idMovieMax, duplicateMovies.Count, firstMovies.Count );

            Dictionary<int, User> firstUsers;
            Dictionary<int, List<User>> duplicateUsers;
            User.ReadUsers( Path.Combine( _badDataPath, "users.dat" ), out firstUsers, out duplicateUsers );
            int idUserMin = firstUsers.Keys.Min();
            int idUserMax = firstUsers.Keys.Max();
            Console.WriteLine( "{3} Users from {0} to {1}, {2} duplicates.", idUserMin, idUserMax, duplicateUsers.Count, firstUsers.Count );

            Dictionary<int,string> badLines;
            int nbRating = User.ReadRatings( Path.Combine( _badDataPath, "ratings.dat" ), firstUsers, firstMovies, out badLines );
            Console.WriteLine( "{0} Ratings: {1} bad lines.", nbRating, badLines.Count );

            Directory.CreateDirectory( _goodDataPath );
            // Saves Movies
            using( TextWriter w = File.CreateText( Path.Combine( _goodDataPath, "movies.dat" ) ) )
            {
                int idMovie = 0;
                foreach( Movie m in firstMovies.Values )
                {
                    m.MovieID = ++idMovie;
                    w.WriteLine( "{0}::{1}::{2}", m.MovieID, m.Title, String.Join( "|", m.Categories ) );
                }
            }

            // Saves Users
            string[] occupations = new string[]{
                "other", 
                "academic/educator", 
                "artist", 
                "clerical/admin",
                "college/grad student",
                "customer service",
                "doctor/health care",
                "executive/managerial",
                "farmer",
                "homemaker",
                "K-12 student",
                "lawyer",
                "programmer",
                "retired",
                "sales/marketing",
                "scientist",
                "self-employed",
                "technician/engineer",
                "tradesman/craftsman",
                "unemployed",
                "writer" };
            using( TextWriter w = File.CreateText( Path.Combine( _goodDataPath, "users.dat" ) ) )
            {
                int idUser = 0;
                foreach( User u in firstUsers.Values )
                {
                    u.UserID = ++idUser;
                    string occupation;
                    int idOccupation;
                    if( int.TryParse( u.Occupation, out idOccupation )
                        && idOccupation >= 0
                        && idOccupation < occupations.Length )
                    {
                        occupation = occupations[idOccupation];
                    }
                    else occupation = occupations[0];
                    w.WriteLine( "{0}::{1}::{2}::{3}::{4}", u.UserID, u.Male ? 'M' : 'F', u.Age, occupation, "US-" + u.ZipCode );
                }
            }
            // Saves Rating
            using( TextWriter w = File.CreateText( Path.Combine( _goodDataPath, "ratings.dat" ) ) )
            {
                foreach( User u in firstUsers.Values )
                {
                    foreach( var r in u.Ratings )
                    {
                        w.WriteLine( "{0}::{1}::{2}", u.UserID, r.Key.MovieID, r.Value );
                    }
                }
            }
        }

        [Test]
        public void ReadMovieData()
        {
            RecoContext c = new RecoContext();
            c.LoadFrom( _goodDataPath );
            for( int i = 0; i < c.Users.Length; ++i )
                Assert.That( c.Users[i].UserID, Is.EqualTo( i + 1 ) );
            for( int i = 0; i < c.Movies.Length; ++i )
                Assert.That( c.Movies[i].MovieID, Is.EqualTo( i + 1 ) );
        }

        [Test]
        public void TestEuclidianDistance()
        {
            var c = new RecoContext();
            c.LoadFrom( _goodDataPath );
            var u1 = c.Users[0];
            var u2 = c.Users[1];
            var u3 = c.Users[2];
            Assert.That( User.EuclidianDistance( u1, u1 ), Is.EqualTo( 0 ) );
            Assert.That( User.EuclidianDistance( u2, u2 ), Is.EqualTo( 0 ) );
            Assert.That( User.EuclidianDistance( u3, u3 ), Is.EqualTo( 0 ) );

        }

        //[Test]
        public void TestAllPearsonSimilarity()
        {
            var c = new RecoContext();
            c.LoadFrom( _goodDataPath );
            foreach( var u1 in c.Users )
            {
                foreach( var u2 in c.Users )
                {
                    double s = c.SimilarityPearson( u1, u2 );
                    Assert.That( !Double.IsNaN( s ) );
                    Assert.That( s >= -1 && s <= 1 );
                    //Assert.That( u1 != u2 || s == 1, "u1 == u2 => s ==1" );
                    Assert.That( s == c.SimilarityPearson( u2, u1 ) );
                }
            }
        }

        [Test, Category( "MyTests" )]
        public void TestAllPearsonSimilaritySubset()
        {
            var c = new RecoContext();
            c.LoadFrom( _subsetDataPath );
            foreach( var u1 in c.Users )
            {
                foreach( var u2 in c.Users )
                {
                    double s = c.SimilarityPearson( u1, u2 );
                    Assert.That( !Double.IsNaN( s ) );
                    Assert.That( s >= -1 && s <= 1 );
                    //Assert.That( u1 != u2 || s == 1, "u1 == u2 => s ==1" );
                    Assert.That( s == c.SimilarityPearson( u2, u1 ) );
                }
            }
        }

        [Test, Category( "MyTests" )]
        public void TestRealSimilarityMovie()
        {
            // Loading a subset of the data
            var c = new RecoContext();
            c.LoadFrom( _subsetDataPath );

            // selecting user 1 and user 2
            var u1 = c.Users[0];
            var u2 = c.Users[1];

            // They must really have the good number of ratings
            Assert.That( u1.Ratings.Count(), Is.EqualTo( 50 ) );
            Assert.That( u2.Ratings.Count(), Is.EqualTo( 50 ) );

            // getting similarity between user 1 and user 2
            double similarity = c.SimilarityPearson( u1, u2 );

            // They must be strictly equals
            Assert.That( similarity, Is.EqualTo( 1 ) );
        }

        [Test, Category( "MyTests" )]
        public void GetRecommendedMovies()
        {
            // Loading a subset of the data
            var c = new RecoContext();
            c.LoadFrom( _subsetDataPath );

            // Getting user 1
            var user = c.Users[0];

            // Getting recommended movies for user 1
            var r = user.RecommendedMovies( c, 15 );

            // Asserts (I detected two non-assigned readonly variable on BestKeeper)
            Assert.That( r.Count, Is.EqualTo( 15 ) );
        }

        [Test, Category( "MyTests" )]
        public void TestRightNumberOfUser()
        {
            // Loading a subset of the data
            var c = new RecoContext();
            c.LoadFrom( _subsetDataPath );

            int users = c.Users.Count();

            Assert.That( users, Is.EqualTo( 5 ) );
        }

        [Test, Category( "MyTests" )]
        public void TestRightNumberOfRatingsForUser1()
        {
            // Loading a subset of the data
            var c = new RecoContext();
            c.LoadFrom( _subsetDataPath );

            // Getting User 1 and his number of ratings (equal to 50)
            var users = c.Users[0];
            int ratingCount = users.Ratings.Count;

            // (I detected ratings are not affected to the correct user, this was offset by a line)
            Assert.That( ratingCount, Is.EqualTo( 50 ) );
        }

        [Test, Category( "MyTests" )]
        public void TestRightNumberOfMovies()
        {
            // Loading a subset of the data
            var c = new RecoContext();
            c.LoadFrom( _subsetDataPath );

            int movies = c.Movies.Count();

            Assert.That( movies, Is.EqualTo( 3883 ) );
        }

        [Test, Category( "MyTests" )]
        public void TestRightMovieSuggested()
        {
            // Loading a subset of the data
            var c = new RecoContext();
            c.LoadFrom( _subsetDataPath );

            // Getting user 5
            var user = c.Users[4];

            // Getting recommended movies for user 1
            var r = user.RecommendedMovies( c, 15 );

            // Getting the user 4
            var user4 = c.Users[3];
            var user4Ratings = user4.Ratings;

            // Compute common movies
            IEnumerable<Movie> common = r.Intersect( user4.Ratings.Keys );

            // Common movies come from user 4 ratings only (16 movies
            Assert.That( common.Count(), Is.EqualTo( 15 ) );

            // There are currently no verification about the film category. Should use the category in the calculation of recommendations.
        }
    }
}
